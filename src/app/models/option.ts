export class Option {
    id: string;
    questionId: number;
    name: string;
    isAnswer: boolean;
    selected: boolean;
    answerNumber: number;


    constructor(data: any) {
        data = data || {};
        this.id = data.id;
        this.questionId = data.questionId;
        this.answerNumber = data.answerNumber;
        this.name = data.name;
        this.selected = data.selected || false;
        this.isAnswer = data.isAnswer;
    }
}
