export class NewUser {
    TenSV: string = '';
    NgaySinh: string = '';
    Email: string = '';
    CMND: string = '';
    DienThoai: string = '';
    DiaChi: string = '';
    KhoiID: number = 0;
    CapID: number = 0;
    DonViID: number = 0;
    CoQuanID: number = 0;
    XaID: number = 0;

    getDateFormat(ngay) {
        //return 'dd/MM/yyyy';
        let date = new Date(ngay);
        return ('0' + date.getDate()).slice(-2) + '/' + ('0' + (date.getMonth() + 1)).slice(-2) + '/' + date.getFullYear();
    }

    constructor(data) {
        this.TenSV = data.hoten || '';
        this.NgaySinh = this.getDateFormat(data.ngaysinh) || '';
        this.Email = data.email || '';
        this.CMND = data.cmnd || '';
        this.DienThoai = data.sodt || '';
        this.DiaChi = data.diachi || '';
        this.KhoiID = data.khoi || 0;
        this.CapID = data.cap || 0;
        this.DonViID = data.donvi || 0;
        this.CoQuanID = data.coquan || 0;
        this.XaID = data.xa || 0;
    }
}
