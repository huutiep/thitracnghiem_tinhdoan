import { Component, OnInit } from '@angular/core';
import { PhapLuatService } from '../../services/phap-luat.service';
import { NzModalService } from 'ng-zorro-antd';
import { Router } from '@angular/router';

@Component({
  selector: 'app-thi-truc-tuyen',
  templateUrl: './thi-truc-tuyen.component.html',
  styleUrls: ['./thi-truc-tuyen.component.css']
})
export class ThiTrucTuyenComponent implements OnInit {

  userInfo: any;
  dataResult: any = [];
  loading: boolean = false;
  dangTaobaiThi: boolean = false;

  logOut() {
    localStorage.removeItem('uid');
    localStorage.removeItem('udata');
    this.router.navigate(['/trang-chu']);
  }

  lamBaiNgay(data) {

    this.router.navigate(['/thong-tin-ky-thi', data.StudentTestID]);
  }

  newExam() {
    this.dangTaobaiThi = true;
    this._sv.postCreateExam(localStorage.getItem('uid')).subscribe((res: any) => {
      if (res.length > 0) {
        const data = res[0];
        console.log(data);
        if (data.ErrCode === 0) {
          this.modalService.confirm({
            nzTitle: 'Thông báo',
            nzContent: data.ErrMsg,
            nzOkText: 'Làm bài ngay',
            nzOkType: 'danger',
            nzOnOk: () => this.lamBaiNgay(data),
            nzCancelText: 'Làm sau',
            nzOnCancel: () => console.log('Cancel')
          });
        } else {
          this.modalService.warning({
            nzTitle: 'Thông báo',
            nzContent: data.ErrMsg
          });
        }
      }
    }, null, () => {
      this.dangTaobaiThi = false;
    })
  }

  getUserInfo() {
    this._sv.getUserInfo(localStorage.getItem('uid')).subscribe((res: any) => {
      if (res.length > 0)
        this.userInfo = res[0]
    })
  }

  getResult() {
    this.loading = true;
    this._sv.getResult(localStorage.getItem('uid')).subscribe((res: any) => {
      if (res.length > 0)
        this.dataResult = res
    }, null, () => {
      this.loading = false;
    })
  }

  constructor(private _sv: PhapLuatService,
    private modalService: NzModalService,
    private router: Router) { }

  ngOnInit() {
    this.getUserInfo();
    this.getResult();
  }



}
