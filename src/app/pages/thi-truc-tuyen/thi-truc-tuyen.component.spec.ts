import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThiTrucTuyenComponent } from './thi-truc-tuyen.component';

describe('ThiTrucTuyenComponent', () => {
  let component: ThiTrucTuyenComponent;
  let fixture: ComponentFixture<ThiTrucTuyenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThiTrucTuyenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThiTrucTuyenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
