import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LamBaiThiComponent } from './lam-bai-thi.component';

describe('LamBaiThiComponent', () => {
  let component: LamBaiThiComponent;
  let fixture: ComponentFixture<LamBaiThiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LamBaiThiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LamBaiThiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
