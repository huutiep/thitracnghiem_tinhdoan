import { Component, OnInit } from '@angular/core';
import { PhapLuatService } from '../../services/phap-luat.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Question } from '../../models/question';
import { Option } from '../../models/option';
import { Quiz } from '../../models/quiz';


@Component({
  selector: 'app-thong-tin-ky-thi',
  templateUrl: './thong-tin-ky-thi.component.html',
  styleUrls: ['./thong-tin-ky-thi.component.css']
})
export class ThongTinKyThiComponent implements OnInit {

  examInfo: any;
  loading: boolean = false;
  myQuiz: Quiz = new Quiz(null);

  constructor(private _sv: PhapLuatService,
    private route: ActivatedRoute,
    private router: Router) { }


  startExam() {
    this.loading = true;
    let tid = +this.route.snapshot.paramMap.get('tid');
    this._sv.postStartExam(tid).subscribe((res: any) => {
      console.log(res)
      if (res.length > 0) {
        // const dataQuiz = this.examInfo;
        // const dataQuestion: Question[] = res[0];
        // const dataAnswer: Option[] = res[1];

        // dataQuestion.forEach(question => {
        //   let options: Option[];
        //   options = dataAnswer.filter(x => {
        //     if (x.questionId === question.id) {
        //       x.name = x.name;
        //       x.answerNumber = +x.id;
        //       x.id = x.questionId + '.' + x.id;
        //       x.selected = +x.selected === 1 ? true : false;
        //       return x;
        //     } else
        //       return false
        //   });
        //   question.questionTypeId = 1;
        //   question.name = question.name
        //   const order = question.order.split('');
        //   let orderedOptions: Option[] = [];
        //   const ABCD = ['A', 'B', 'C', 'D']
        //   order.forEach((ord, index) => {
        //     let opt = options.find(x => x.answerNumber === +ord)
        //     opt.name = ABCD[index] + '. ' + this._sv.b64DecodeUnicode(opt.name);
        //     orderedOptions.push(opt)
        //   });
        //   question.options = orderedOptions;
        // });

        // this.myQuiz = new Quiz({ id: dataQuiz.StudentTestID, questions: dataQuestion, remaintime: res[2][0].RemainTime, name: dataQuiz.ExamName, soCauHoi: dataQuiz.SoCauHoi, thoiGian: dataQuiz.ThoiGian })
        // localStorage.setItem('udata', JSON.stringify(this.myQuiz));
        this.loading = false;
        this.router.navigate(['/lam-bai-thi', tid]);
      }
    }, null, () => {
      this.loading = false;
    })
  }

  ngOnInit() {
    localStorage.removeItem('udata');
    this.getExamInfo();
  }

  getExamInfo() {
    let tid = +this.route.snapshot.paramMap.get('tid');
    this._sv.getExamInfo(tid).subscribe((res: any) => {
      if (res.length > 0) {
        this.examInfo = res[0];
      }
    })
  }

}
