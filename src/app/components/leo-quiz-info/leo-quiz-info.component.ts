import { Component, OnInit, Input } from '@angular/core';
import { Question } from '../../models/question';

@Component({
  selector: 'app-leo-quiz-info',
  templateUrl: './leo-quiz-info.component.html',
  styleUrls: ['./leo-quiz-info.component.css']
})
export class LeoQuizInfoComponent implements OnInit {
  @Input() data: Question[];


  phanTramHoanThanh() {
    if (this.data)
      return Math.round((this.data.filter(q => +q.answered > 0).length / this.data.length) * 100);
    return 0
  }



  constructor() { }

  ngOnInit() {
  }



}
