import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeoQuizComponent } from './leo-quiz.component';

describe('LeoQuizComponent', () => {
  let component: LeoQuizComponent;
  let fixture: ComponentFixture<LeoQuizComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeoQuizComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeoQuizComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
