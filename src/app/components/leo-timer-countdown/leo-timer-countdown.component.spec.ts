import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeoTimerCountdownComponent } from './leo-timer-countdown.component';

describe('LeoTimerCountdownComponent', () => {
  let component: LeoTimerCountdownComponent;
  let fixture: ComponentFixture<LeoTimerCountdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeoTimerCountdownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeoTimerCountdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
