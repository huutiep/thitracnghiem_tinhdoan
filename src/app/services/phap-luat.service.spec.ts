import { TestBed, inject } from '@angular/core/testing';

import { PhapLuatService } from './phap-luat.service';

describe('PhapLuatService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PhapLuatService]
    });
  });

  it('should be created', inject([PhapLuatService], (service: PhapLuatService) => {
    expect(service).toBeTruthy();
  }));
});
